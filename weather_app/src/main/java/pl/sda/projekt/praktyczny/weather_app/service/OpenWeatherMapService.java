package pl.sda.projekt.praktyczny.weather_app.service;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import pl.sda.projekt.praktyczny.weather_app.http.ObjectMapperGsonImpl;
import pl.sda.projekt.praktyczny.weather_app.model.OpenWeatherMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class OpenWeatherMapService {

    private final String APP_ID;

    public OpenWeatherMapService() throws IOException {
        // read app_id from properties file
        final InputStream resourceAsStream = OpenWeatherMapService.class.getClassLoader().getResourceAsStream("open_weather_map.properties");
        if (resourceAsStream == null) {
            throw new RuntimeException("Missing open_weather_map.properties file containing app_id for OpenWeatherMap API");
        }
        Properties properties = new Properties();
        properties.load(resourceAsStream);
        APP_ID = properties.getProperty("app_id");

        if (APP_ID == null || APP_ID.isEmpty()) {
            throw new RuntimeException("Missing app_id for OpenWeatherMap API in open_weather_map.properties");
        }

        // initialize Unirest
        Unirest.setObjectMapper(new ObjectMapperGsonImpl());
    }

    public OpenWeatherMap getWeather(String city) throws UnirestException {

        final OpenWeatherMap weatherForSelectedCity = Unirest.get("https://api.openweathermap.org/data/2.5/weather")
                .queryString("q", city)
                .queryString("appid", APP_ID)
                .queryString("units", "metric")
                .asObject(OpenWeatherMap.class)
                .getBody();

        return weatherForSelectedCity;
    }

}
