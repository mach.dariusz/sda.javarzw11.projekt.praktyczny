package pl.sda.projekt.praktyczny.weather_app.service;

import com.mashape.unirest.http.exceptions.UnirestException;
import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;
import pl.sda.projekt.praktyczny.weather_app.repository.WeatherRepository;

import java.time.LocalDate;
import java.util.List;

public class WeatherService {

    private final WeatherRepository weatherRepository;

    public WeatherService(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    public List<CityForMenu> getCities() {
        return weatherRepository.fetchListOfCitiesWithRecords();
    }

    public void updateCityInformation(String city) throws UnirestException {
        weatherRepository.updateWeatherDataForCity(city);
    }

    public double getAverageTempForSelectedCity(String city) {
        final List<Weather> weatherDataByCityForToday = weatherRepository.getWeatherDataByCityAndDate(city, LocalDate.now());

        return weatherDataByCityForToday.stream()
                .map(weather -> weather.getTemp())
                .mapToDouble(o -> o)
                .average()
                .getAsDouble();
    }

}
