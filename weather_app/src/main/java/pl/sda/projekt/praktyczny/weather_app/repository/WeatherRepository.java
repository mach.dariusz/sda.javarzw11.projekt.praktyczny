package pl.sda.projekt.praktyczny.weather_app.repository;

import com.mashape.unirest.http.exceptions.UnirestException;
import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;

import java.time.LocalDate;
import java.util.List;

public interface WeatherRepository {

    /**
     * Returns weather data for selected city by date
     *
     * @param city
     * @param date
     * @return weather data items
     */
    List<Weather> getWeatherDataByCityAndDate(String city, LocalDate date);

    /**
     * Updates weather data for selected city
     *
     * @param city
     */
    void updateWeatherDataForCity(String city) throws UnirestException;

    List<CityForMenu> fetchListOfCitiesWithRecords();

}
