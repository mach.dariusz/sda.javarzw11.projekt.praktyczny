package pl.sda.projekt.praktyczny.weather_app.model;

import lombok.Data;

@Data
public class OpenWeatherMapCoord {

    private double lon;
    private double lat;

}
