package pl.sda.projekt.praktyczny.weather_app.utils;

import com.mashape.unirest.http.exceptions.UnirestException;
import pl.sda.projekt.praktyczny.weather_app.dao.WeatherDaoImpl;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;
import pl.sda.projekt.praktyczny.weather_app.repository.WeatherRepositoryImpl;
import pl.sda.projekt.praktyczny.weather_app.service.WeatherService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MenuUtils {

    private static WeatherService weatherService;

    public static void showMenu() {

        try {
            weatherService = new WeatherService(new WeatherRepositoryImpl(new WeatherDaoImpl(JpaUtils.getEntityManagerInstance())));

            int selectedOption = -1;
            Scanner scanner = new Scanner(System.in);
            do {
                // display menu
                if (selectedOption != -1) {
                    display("#");
                    display("###");
                    display("#####");
                    display("Welcome back in main menu");
                } else {
                    display("Hello in Weather application");
                }

                display("");
                display("Please select one of below options: [0-10]");
                display("\t 1. Show weather for selected city");
                display("\t 2. Update weather for selected city");
                display("\t 3. Display all cities with weather records");
                display("");
                display("\t 0. Exit");

                try {
                    selectedOption = scanner.nextInt();
                } catch (InputMismatchException ex) {
                    scanner = new Scanner(System.in);
                    selectedOption = handleWrongInput();
                }


                switch (selectedOption) {
                    case 0:
                        display("Thank you spending time with us!");
                        display("#####");
                        display("###");
                        display("#");
                        return;

                    case 1:
                        getWeatherForSelectedCity();
                        break;
                    case 2:
                        updateWeatherForSelectedCity();
                        break;
                    case 3:
                        showCitiesWithRecordsInDB();
                        break;

                    default:
                        display("Sorry, we don't support this option, please select again ...");
                        break;
                }

            } while (true);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

    }

    public static void getWeatherForSelectedCity() {
        do {
            display("Which city would you like to check?");
            showCitiesWithRecordsInDB();
            Scanner cityScanner = new Scanner(System.in);
            final String cityInput = cityScanner.next();
            double averageTemp = weatherService.getAverageTempForSelectedCity(cityInput);

            display(String.format("Average temperature for today [%s] is %s degrees Celsius", LocalDate.now(), averageTemp));

            return;
        } while (true);
    }

    public static void updateWeatherForSelectedCity() throws UnirestException {
        do {
            display("Which city would you like to update?");
            Scanner cityScanner = new Scanner(System.in);
            final String cityInput = cityScanner.next();
            weatherService.updateCityInformation(cityInput);

            display("");
            display("Database should be updated, please check list of cities ...");
            return;
        } while (true);
    }

    public static void showCitiesWithRecordsInDB() {
        do {
            display("List of cities:");
            final List<CityForMenu> cities = weatherService.getCities();

            if (cities.size() == 0) {
                display("I am sorry, but we doesn't have any records in our database. \nPlease update city weather information. \nThank you for your understanding!");
            }

            for (int i = 0; i < cities.size(); i++) {
                display(String.format("%d. %s %s", (i + 1), cities.get(i).getCityFromApi(), cities.get(i).getCitiesFromRequest()));
            }

            return;
        } while (true);
    }

    public static String toCamelCase(String word) {
        if (word != null && !word.isEmpty() && word.length() > 0) {
            return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
        }

        return word;
    }

    private static int handleWrongInput() {
        display("Please select valid option: [0-10]");
        Scanner scanner = new Scanner(System.in);

        do {
            try {
                return scanner.nextInt();
            } catch (InputMismatchException ex) {
                return handleWrongInput();
            }
        } while (true);
    }

    private static void display(String sentence) {
        System.out.println(sentence);
    }

}
