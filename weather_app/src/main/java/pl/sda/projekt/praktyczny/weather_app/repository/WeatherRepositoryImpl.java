package pl.sda.projekt.praktyczny.weather_app.repository;

import com.mashape.unirest.http.exceptions.UnirestException;
import pl.sda.projekt.praktyczny.weather_app.dao.WeatherDao;
import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;
import pl.sda.projekt.praktyczny.weather_app.model.OpenWeatherMap;
import pl.sda.projekt.praktyczny.weather_app.service.OpenWeatherMapService;
import pl.sda.projekt.praktyczny.weather_app.utils.WeatherUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class WeatherRepositoryImpl implements WeatherRepository {

    private final WeatherDao weatherDao;
    private final OpenWeatherMapService openWeatherMapService;

    public WeatherRepositoryImpl(WeatherDao weatherDao) throws IOException {
        this.weatherDao = weatherDao;
        this.openWeatherMapService = new OpenWeatherMapService();
    }

    @Override
    public List<Weather> getWeatherDataByCityAndDate(String city, LocalDate date) {
        return weatherDao.findByCityAndDate(city, date);
    }

    @Override
    public void updateWeatherDataForCity(String city) throws UnirestException {
        final OpenWeatherMap weatherForSelectedCity = openWeatherMapService.getWeather(city);
        weatherDao.save(WeatherUtils.openWeatherMapToWeatherMapper(city, weatherForSelectedCity, LocalDate.now()));
    }

    @Override
    public List<CityForMenu> fetchListOfCitiesWithRecords() {
        return weatherDao.findAllDistinctCities();
    }

}
