package pl.sda.projekt.praktyczny.weather_app.dao;

import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;

import java.time.LocalDate;
import java.util.List;

public interface WeatherDao {

    // save Weather
    void save(Weather weather);

    // save all Weather objects
    void saveAll(Weather... weathers);

    // read Weather items by city and date
    List<Weather> findByCityAndDate(String city, LocalDate date);

    List<CityForMenu> findAllDistinctCities();
}
