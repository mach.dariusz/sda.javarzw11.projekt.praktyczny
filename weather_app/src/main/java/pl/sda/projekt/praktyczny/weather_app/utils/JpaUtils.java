package pl.sda.projekt.praktyczny.weather_app.utils;

import lombok.extern.java.Log;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Log
public class JpaUtils {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public static EntityManager getEntityManagerInstance() {
        if (entityManagerFactory == null) {
            setupDBConnection();
        }

        if (entityManager == null) {
            entityManager = entityManagerFactory.createEntityManager();
        }

        return entityManager;
    }

    public static void setupDBConnection() {
        entityManagerFactory = Persistence.createEntityManagerFactory("pl.sda.projekt.praktyczny.weather_app");
    }

    public static void closeConnectionToDB() {
        if (entityManager != null) {
            entityManager.close();
            entityManager = null;
            log.info("entityManager closed");
        }

        if (entityManagerFactory != null) {
            entityManagerFactory.close();
            entityManagerFactory = null;
            log.info("EntityManagerFactory closed");
        }

        log.info("Connection to db should closed now");
    }
}
