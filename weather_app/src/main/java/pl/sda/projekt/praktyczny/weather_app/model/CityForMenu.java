package pl.sda.projekt.praktyczny.weather_app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityForMenu {

    private String cityFromApi;
    private List<String> citiesFromRequest;

}
