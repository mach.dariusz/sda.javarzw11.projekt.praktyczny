package pl.sda.projekt.praktyczny.weather_app;

import lombok.extern.java.Log;
import pl.sda.projekt.praktyczny.weather_app.utils.JpaUtils;
import pl.sda.projekt.praktyczny.weather_app.utils.MenuUtils;

@Log
public class Application {

    public static void main(String[] args) {

        try {
            MenuUtils.showMenu();
        } finally {
            JpaUtils.closeConnectionToDB();
        }

    }

}
