package pl.sda.projekt.praktyczny.weather_app.utils;

import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.OpenWeatherMap;

import java.time.LocalDate;

public class WeatherUtils {

    public static Weather openWeatherMapToWeatherMapper(String city, OpenWeatherMap openWeatherMap, LocalDate date) {
        return Weather.builder()
                .cityFromApi(openWeatherMap.getName())
                .cityFromRequest(city)
                .temp(openWeatherMap.getMain().getTemp())
                .temp_min(openWeatherMap.getMain().getTemp_min())
                .temp_max(openWeatherMap.getMain().getTemp_max())
                .pressure(openWeatherMap.getMain().getPressure())
                .date(date)
                .country(openWeatherMap.getSys().getCountry())
                .lat(openWeatherMap.getCoord().getLat())
                .lon(openWeatherMap.getCoord().getLon())
                .build();
    }
}
