package pl.sda.projekt.praktyczny.weather_app.dao;

import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;
import pl.sda.projekt.praktyczny.weather_app.utils.MenuUtils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class WeatherDaoImpl implements WeatherDao {

    private final EntityManager entityManager;

    public WeatherDaoImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Weather weather) {
        entityManager.getTransaction().begin();

        persistOrMerge(weather);

        entityManager.getTransaction().commit();
    }

    @Override
    public void saveAll(Weather... weathers) {
        entityManager.getTransaction().begin();

        for (int i = 0; i < weathers.length; i++) {
            persistOrMerge(weathers[i]);
        }

        entityManager.getTransaction().commit();
    }

    @Override
    public List<Weather> findByCityAndDate(String city, LocalDate date) {
        final String query = "SELECT w FROM Weather w WHERE (w.cityFromApi = :city OR w.cityFromRequest = :city) AND w.date = :date";
        final List<Weather> weatherItems = entityManager
                .createQuery(query, Weather.class)
                .setParameter("city", city)
                .setParameter("date", date)
                .getResultList();

        return weatherItems;
    }

    @Override
    public List<CityForMenu> findAllDistinctCities() {

        final String query = "SELECT w.cityFromApi, w.cityFromRequest FROM Weather w";
        final List<Object[]> resultList = entityManager.createQuery(query).getResultList();

        Map<String, Set<String>> citiesWithAliases = new HashMap<>();
        resultList.stream()
                .map(listOfObjects -> Arrays.asList(listOfObjects))
                .forEach(listOfObjects -> {
                    String cityFromApi = (String) listOfObjects.get(0);
                    String cityFromRequest = MenuUtils.toCamelCase((String) listOfObjects.get(1));

                    if (citiesWithAliases.containsKey(cityFromApi)) {
                        // update list of aliases
                        final Set<String> aliases = citiesWithAliases.get(cityFromApi);
                        aliases.add(cityFromRequest);
                        citiesWithAliases.put(cityFromApi, aliases);
                    } else {
                        // put key and alias in list
                        final Set<String> aliases = new HashSet<>();
                        aliases.add(cityFromRequest);
                        citiesWithAliases.put(cityFromApi, aliases);
                    }
                });

        return citiesWithAliases.entrySet().stream()
                .map(entries -> CityForMenu.builder()
                        .cityFromApi(entries.getKey())
                        .citiesFromRequest(new ArrayList<>(entries.getValue()))
                        .build())
                .collect(Collectors.toList());
    }

    private void persistOrMerge(Weather weather) {
        if (weather.getId() == null) {
            entityManager.persist(weather);
        } else {
            entityManager.merge(weather);
        }
    }

}
