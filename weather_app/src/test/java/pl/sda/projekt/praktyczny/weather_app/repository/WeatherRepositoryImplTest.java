package pl.sda.projekt.praktyczny.weather_app.repository;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.jupiter.api.*;
import pl.sda.projekt.praktyczny.weather_app.dao.WeatherDao;
import pl.sda.projekt.praktyczny.weather_app.dao.WeatherDaoImpl;
import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.utils.JpaUtils;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

class WeatherRepositoryImplTest {

    private static EntityManager entityManager;
    private static WeatherRepository weatherRepository;

    @BeforeAll
    static void setup() throws IOException {
        entityManager = JpaUtils.getEntityManagerInstance();
        WeatherDao weatherDao = new WeatherDaoImpl(entityManager);
        weatherRepository = new WeatherRepositoryImpl(weatherDao);
    }

    @AfterAll
    static void cleaning() {
        // use it for cleaning stuff
        JpaUtils.closeConnectionToDB();
    }

    @BeforeEach
    void cleanDB() {
        entityManager.getTransaction().begin();
        entityManager.createQuery("DELETE FROM Weather").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    void getWeatherDataByCityAndDate_noDataInDB_emptyListReturned() {
        final List<Weather> weatherItemsForRzeszow = weatherRepository
                .getWeatherDataByCityAndDate("Rzeszów", LocalDate.now());

        Assertions.assertEquals(weatherItemsForRzeszow.size(), 0);
    }

    @Test
    void getWeatherDataByCityAndDate_dataSavedInDB_areReturned() throws UnirestException {
        // load for Rzeszow 3 times
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        // load for Warszawa 2 times
        weatherRepository.updateWeatherDataForCity("Warszawa");
        weatherRepository.updateWeatherDataForCity("Warszawa");

        final List<Weather> weatherItemsForRzeszow = weatherRepository
                .getWeatherDataByCityAndDate("Rzeszów", LocalDate.now());

        final List<Weather> weatherItemsForWarsaw = weatherRepository
                .getWeatherDataByCityAndDate("Warszawa", LocalDate.now());

        final List<Weather> weatherItemsForCracow = weatherRepository
                .getWeatherDataByCityAndDate("Kraków", LocalDate.now());

        Assertions.assertEquals(weatherItemsForRzeszow.size(), 3);
        Assertions.assertEquals(weatherItemsForWarsaw.size(), 2);
        Assertions.assertEquals(weatherItemsForCracow.size(), 0);
    }

    @Test
    void updateWeatherDataForCity_noDataInDB_isReturned() throws UnirestException {
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        final List<Weather> weatherItemsForRzeszow = weatherRepository
                .getWeatherDataByCityAndDate("Rzeszów", LocalDate.now());

        Assertions.assertEquals(weatherItemsForRzeszow.size(), 1);
    }

    @Test
    void updateWeatherDataForCity_dataSavedInDB_areReturned() throws UnirestException {
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszów"); // 5 times
        weatherRepository.updateWeatherDataForCity("Żołynia");
        weatherRepository.updateWeatherDataForCity("Żołynia"); // 2 times
        weatherRepository.updateWeatherDataForCity("Sosnowiec");
        weatherRepository.updateWeatherDataForCity("Sosnowiec");
        weatherRepository.updateWeatherDataForCity("Sosnowiec"); // 3 times
        weatherRepository.updateWeatherDataForCity("Radom"); // 1 times

        final List<Weather> weatherItemsForRzeszow = weatherRepository.getWeatherDataByCityAndDate("Rzeszów", LocalDate.now());
        final List<Weather> weatherItemsForZolynia = weatherRepository.getWeatherDataByCityAndDate("Żołynia", LocalDate.now());
        final List<Weather> weatherItemsForSosnowiec = weatherRepository.getWeatherDataByCityAndDate("Sosnowiec", LocalDate.now());
        final List<Weather> weatherItemsForRadom = weatherRepository.getWeatherDataByCityAndDate("Radom", LocalDate.now());
        final List<Weather> weatherItemsForWarszawa = weatherRepository.getWeatherDataByCityAndDate("Warszawa", LocalDate.now());
        final List<Weather> weatherItemsForKoszalin = weatherRepository.getWeatherDataByCityAndDate("Koszalin", LocalDate.now());

        Assertions.assertEquals(weatherItemsForRzeszow.size(), 5);
        Assertions.assertEquals(weatherItemsForZolynia.size(), 2);
        Assertions.assertEquals(weatherItemsForSosnowiec.size(), 3);
        Assertions.assertEquals(weatherItemsForRadom.size(), 1);
        Assertions.assertEquals(weatherItemsForWarszawa.size(), 0);
        Assertions.assertEquals(weatherItemsForKoszalin.size(), 0);
    }

}