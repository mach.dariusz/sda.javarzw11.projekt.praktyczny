package pl.sda.projekt.praktyczny.weather_app.service;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.jupiter.api.*;
import pl.sda.projekt.praktyczny.weather_app.model.OpenWeatherMap;

import java.io.IOException;

class OpenWeatherMapServiceTest {

    private static OpenWeatherMapService service;

    @BeforeAll
    static void setup() throws IOException {
        service = new OpenWeatherMapService();
    }

    @AfterAll
    static void cleaning() {
        // use it for cleaning stuff
    }

    @Test
    @DisplayName("test calling OpenWeatherMap API")
    void test_calling_OpenWeatherMap_API() throws UnirestException {
        final OpenWeatherMap weatherForRzeszow = service.getWeather("Rzeszów");

        Assertions.assertEquals(weatherForRzeszow.getName(), "Rzeszów");
        Assertions.assertNotNull(weatherForRzeszow.getCoord());
        Assertions.assertEquals(weatherForRzeszow.getSys().getCountry(), "PL");
        Assertions.assertTrue(weatherForRzeszow.getMain().getTemp() > 0.0);
    }

}