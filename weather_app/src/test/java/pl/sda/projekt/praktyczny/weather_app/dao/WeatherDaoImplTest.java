package pl.sda.projekt.praktyczny.weather_app.dao;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.jupiter.api.*;
import pl.sda.projekt.praktyczny.weather_app.entity.Weather;
import pl.sda.projekt.praktyczny.weather_app.model.CityForMenu;
import pl.sda.projekt.praktyczny.weather_app.repository.WeatherRepository;
import pl.sda.projekt.praktyczny.weather_app.repository.WeatherRepositoryImpl;
import pl.sda.projekt.praktyczny.weather_app.utils.JpaUtils;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

public class WeatherDaoImplTest {


    @BeforeAll
    static void setup() {
    }

    @AfterAll
    static void cleaning() {
        // use it for cleaning stuff
        JpaUtils.closeConnectionToDB();
    }

    @BeforeEach
    void cleanDB() {
        final EntityManager entityManager = JpaUtils.getEntityManagerInstance();
        entityManager.getTransaction().begin();
        entityManager.createQuery("DELETE FROM Weather").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @DisplayName("Testing if Weather object is properly saved in database")
    @Test
    void saveWeather_notExistedInDB_savedInDB() {

        //given
        final EntityManager entityManager = JpaUtils.getEntityManagerInstance();
        WeatherDao weatherDao = new WeatherDaoImpl(entityManager);
        Weather weather = new Weather();
        weather.setDate(LocalDate.now());
        weather.setCityFromApi("Rzeszów");
        weather.setTemp(20.00);

        //when
        weatherDao.save(weather);

        //then
        final List<Weather> weatherItems = entityManager.createQuery(
                "SELECT w FROM Weather w WHERE w.cityFromApi = :city", Weather.class)
                .setParameter("city", "Rzeszów")
                .getResultList();

        Assertions.assertTrue(weatherItems.size() == 1);
        Assertions.assertEquals(weatherItems.stream().findFirst().get().getCityFromApi(), "Rzeszów");
    }

    @DisplayName("Testing if Weather items are fetched from database using city and date in query")
    @Test
    void findWeatherByCityAndDate_existedInDB_areReturned() {
        //BDD approach
        //given
        final EntityManager entityManager = JpaUtils.getEntityManagerInstance();
        WeatherDao weatherDao = new WeatherDaoImpl(entityManager);
        final Weather weather1 = Weather.builder().cityFromApi("Rzeszów").date(LocalDate.now()).temp(20.00).build();
        final Weather weather2 = Weather.builder().cityFromApi("Rzeszów").date(LocalDate.now()).temp(22.00).build();
        final Weather weather3 = Weather.builder().cityFromApi("Rzeszów").date(LocalDate.now()).temp(23.00).build();
        final Weather weather4 = Weather.builder().cityFromApi("Rzeszów").date(LocalDate.now()).temp(21.00).build();
        final Weather weather5 = Weather.builder().cityFromApi("Rzeszów").date(LocalDate.now()).temp(24.00).build();
        final Weather weather6_another_day = Weather.builder().cityFromApi("Rzeszów").date(LocalDate.now().plus(1, DAYS)).temp(24.00).build();
        final Weather weather7_another_city = Weather.builder().cityFromApi("Warszawa").date(LocalDate.now()).temp(24.00).build();

        //when
        weatherDao.saveAll(weather1, weather2, weather3, weather4, weather5, weather6_another_day, weather7_another_city);

        //then
        final List<Weather> weatherForRzeszow = weatherDao.findByCityAndDate("Rzeszów", LocalDate.now());
        Assertions.assertEquals(weatherForRzeszow.size(), 5);

        final List<Weather> weatherForRzeszowTomorrow = weatherDao.findByCityAndDate("Rzeszów", LocalDate.now().plus(1, DAYS));
        Assertions.assertEquals(weatherForRzeszowTomorrow.size(), 1);

        final List<Weather> weatherForWarszawa = weatherDao.findByCityAndDate("Warszawa", LocalDate.now());
        Assertions.assertEquals(weatherForWarszawa.size(), 1);

        final List<Weather> weatherForKrakow = weatherDao.findByCityAndDate("Kraków", LocalDate.now());
        Assertions.assertEquals(weatherForKrakow.size(), 0);
    }

    @Test
    void findAllDistinctCities() throws IOException, UnirestException {
        final EntityManager entityManager = JpaUtils.getEntityManagerInstance();
        WeatherDao weatherDao = new WeatherDaoImpl(entityManager);

        final WeatherRepository weatherRepository = new WeatherRepositoryImpl(weatherDao);
        weatherRepository.updateWeatherDataForCity("Rzeszów");
        weatherRepository.updateWeatherDataForCity("Rzeszow");
        weatherRepository.updateWeatherDataForCity("Warszawa");
        weatherRepository.updateWeatherDataForCity("Warsaw");

        final List<CityForMenu> allDistinctCities = weatherDao.findAllDistinctCities();

        Assertions.assertTrue(allDistinctCities.size() > 0);
    }

}
