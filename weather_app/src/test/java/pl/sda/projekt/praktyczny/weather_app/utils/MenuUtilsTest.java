package pl.sda.projekt.praktyczny.weather_app.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MenuUtilsTest {

    @Test
    void testCamelCase() {
        final String nullString = MenuUtils.toCamelCase(null);
        final String aString = MenuUtils.toCamelCase("a");
        final String rzeszowString = MenuUtils.toCamelCase("rzeszów");
        final String rzeszowCamelString = MenuUtils.toCamelCase("Rzeszów");

        Assertions.assertEquals(nullString, null);
        Assertions.assertEquals(aString, "A");
        Assertions.assertEquals(rzeszowString, "Rzeszów");
        Assertions.assertEquals(rzeszowCamelString, "Rzeszów");
    }

}